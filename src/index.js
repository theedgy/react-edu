import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers/index';

import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'normalize.css';
import './index.css';

const store = createStore(reducers);

ReactDOM.render(
	<Provider store={ store }>
		<App />
	</Provider>
	,document.getElementById('root')
);
