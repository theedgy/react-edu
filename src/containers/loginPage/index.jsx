import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import { Container } from 'reactstrap';

import LoginForm from '../../components/loginForm';
import { isLoggedIn, setCookie, authApp } from '../../helpers';

class Login extends Component {
	static propTypes = {
		match: PropTypes.object.isRequired,
		location: PropTypes.object.isRequired,
		history: PropTypes.object.isRequired
	};

	state = {
		userName: '',
		password: '',
		allowLogin: false
	};

	changeName = (e) => {
		this.setState({
			userName: e.target.value,
			allowLogin: e.target.value !== '' && this.state.password !== ''
		})
	};

	changePass = (e) => {
		this.setState({
			password: e.target.value,
			allowLogin: this.state.userName !== '' && e.target.value !== ''
		})
	};

	handleSubmit = () => {
		authApp().then(response => {
			if (response.status === 201) {
				setCookie('token', response.data.token);
				this.forceUpdate();
			}
		})

	};

	render() {
		if (isLoggedIn()) {
			return <Redirect to='/posts'/>
		}

		return(
			<Container>
				<LoginForm
					userName={ this.state.userName }
					password={ this.state.password }
					changeName={ this.changeName }
					changePass={ this.changePass }
					handleSubmit={ this.handleSubmit }
					allowLogin={ this.state.allowLogin } />
			</Container>
		)
	}
}

export default Login;
