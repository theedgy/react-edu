import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row } from 'reactstrap'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';

import ModalElement from '../../components/modal';
import AuthorInfo from '../../components/authorInfo';
import FiltersArticles from '../../components/filtersArticles';
import PostsPagination from '../../components/pagination';
import PostListElement from '../../components/postListElement';
import PostGridElement from '../../components/postGridElement';
import Loader from '../../components/loader';
import { updateDisplayPosts } from '../../actions/index';
import { defaultParams, getPosts, isLoggedIn, URLparams } from '../../helpers';
import './style.css';

class Articles extends Component {
	static propTypes = {
		match: PropTypes.object.isRequired,
		location: PropTypes.object.isRequired,
		allPosts: PropTypes.array.isRequired,
		displayPosts: PropTypes.array.isRequired,
		layout: PropTypes.string.isRequired,
		updateDisplayPosts: PropTypes.func.isRequired,
	};

	state = {
		modalOpen: false,
		author: {}
	};

	params = {
		page: this.props.match.params.page ? parseInt(this.props.match.params.page, 10) : defaultParams.page
	};

	componentDidMount = () => {
		this.fetchPosts(this.params);
	};

	shouldComponentUpdate = (nextProps) => {
		const currentPage = this.props.match.params.page ? this.props.match.params.page : defaultParams.page;
		this.params.page = nextProps.match.params.page ? nextProps.match.params.page : defaultParams.page;

		const currentOrder = URLparams(undefined, this.props.location.search).get('order');
		this.params.order = URLparams(undefined, nextProps.location.search).get('order');

		const currentSortBy = URLparams(undefined, this.props.location.search).get('sortBy');
		this.params.sortBy = URLparams(undefined, nextProps.location.search).get('sortBy');

		if (parseInt(this.params.page, 10) !== parseInt(currentPage, 10)
			|| this.params.order !== currentOrder
			|| this.params.sortBy !== currentSortBy) {

			this.fetchPosts(this.params);
			return false
		}

		return true
	};

	fetchPosts = (data) => {
		getPosts(data).then(response => (
			this.props.updateDisplayPosts({ posts: response.data })
		));
	};

	toggleModal = (id) => {
		this.setState({
			modalOpen: !this.state.modalOpen,
			author: id ? this.props.allPosts.find(p => p.id === id).author[ 0 ] : {}
		});
	};

	render() {
		if (!isLoggedIn()) {
			return <Redirect to='/'/>
		}

		const { allPosts, displayPosts, match, layout } = this.props;
		const pages = Math.ceil(allPosts.length/defaultParams.limit);
		const page = parseInt(match.params.page ? match.params.page : defaultParams.page, 10);
		const PostComponent = layout === 'list' ? PostListElement : PostGridElement;

		if (displayPosts.length === 0 || allPosts.length === 0 ) {
			return <Loader />
		}

		return(
			<Container>

				<FiltersArticles />

				<Row>
					{ displayPosts.map(el => <PostComponent post={ el } key={ el.id } handleModal={ this.toggleModal } /> ) }
				</Row>

				<PostsPagination pages={ pages } page={ page } />

				<ModalElement open={ this.state.modalOpen }>
					<AuthorInfo author={ this.state.author } handleToggle={ this.toggleModal } />
				</ModalElement>

			</Container>
		);
	}
}

const mapStateToProps = state => ({
	allPosts: state.allPosts,
	displayPosts: state.displayPosts,
	layout: state.layout,
});

const mapDispatchToProps = dispatch => ( bindActionCreators({ updateDisplayPosts } ,dispatch) );

export default connect(mapStateToProps, mapDispatchToProps)(Articles);
