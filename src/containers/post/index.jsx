import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Container, Col, Button, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';

import ModalElement from '../../components/modal';
import CommentModal from '../../components/commentModal';
import AuthorInfo from '../../components/authorInfo';
import Loader from '../../components/loader';
import { getPost, getPostDate, isLoggedIn, sendTime } from '../../helpers';
import './style.css';

class SinglePost extends Component {
	static propTypes = {
		allPosts: PropTypes.array.isRequired,
		match: PropTypes.object.isRequired,
		comments: PropTypes.array
	};

	static defaultProps = {
		comments: []
	};

	state = {
		postElement : {},
		authorModal: false,
		commentModal: false,
		startTimer: 0
	};

	componentDidMount = () => {
		this.setState({ startTimer: Math.round(new Date() / 1000) });

		const post = this.props.allPosts && this.props.allPosts.find(p => p.id === this.props.match.params.id );

		if (post) {
			return this.setState({ postElement : post })
		}
		else {
			getPost(this.props.match.params.id).then(response => (
					this.setState({ postElement : response.data })
				)
			)
		}
	};

	componentWillUnmount = () => {
		sendTime(this.props.match.params.id, Math.round(new Date() / 1000) - this.state.startTimer)
			.then(response => response.status === 200 && console.log(`Gratulacje, oglądałeś post przez: ${ response.data.params.time } sekund`) )
	};

	commentModal = () => this.setState({ commentModal: !this.state.commentModal });

	authorModal = () => this.setState({ authorModal: !this.state.authorModal });

	render() {
		if (!isLoggedIn()) {
			return <Redirect to='/'/>
		}

		if (!this.state.postElement || Object.keys(this.state.postElement).length === 0) {
			return <Loader />
		}

		const { id, title, thumbnail, date, content, author } = this.state.postElement;

		return(
			<Container>
				<Col lg={ { size: 8, offset: 2 } }>
					<article className="post-article">
						<h2>{ title }</h2>
						<img src={ thumbnail } alt={ title } />
						<div className="post-intro">
							<span>{ getPostDate(date) }</span>
							<Button color="warning" onClick={ this.authorModal }>i</Button>
						</div>

						<div className="post-content">
							{ content }
						</div>

						<Button className="comment-trigger" onClick={ this.commentModal }>Comment</Button>

						{this.props.comments.length > 0 && (
							<ListGroup>
								{ this.props.comments.map((comment, i) => {
									if (parseInt(comment.pID, 10) === parseInt(this.props.match.params.id, 10)) {
										return (
											<ListGroupItem key={ `${ i }-${ comment.pID }` }>
												<ListGroupItemHeading>{ comment.comment }</ListGroupItemHeading>
												<ListGroupItemText>{ comment.name }</ListGroupItemText>
											</ListGroupItem>
										)
									}
									return false
								} ) }
							</ListGroup>
						) }

						<ModalElement open={ this.state.authorModal }>
							<AuthorInfo author={ author[ 0 ] } handleToggle={ this.authorModal } />
						</ModalElement>

						<ModalElement open={ this.state.commentModal }>
							<CommentModal post={ parseInt(id, 10) }  handleToggle={ this.commentModal }/>
						</ModalElement>
					</article>
				</Col>
			</Container>
		)
	}
}

const mapStateToProps = state => ({
	allPosts: state.allPosts,
	comments: state.comments
});

export default connect(mapStateToProps)(SinglePost);
