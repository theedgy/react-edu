import { combineReducers } from 'redux';

import PostReducer from './postReducer';
import DisplayPostReducer from './displayPostReducer';
import LayoutReducer from './layoutReducer';
import CommetReducer from './commentsReducer';

const reducers = combineReducers({
	allPosts: PostReducer,
	displayPosts: DisplayPostReducer,
	layout: LayoutReducer,
	comments: CommetReducer,
});

export default reducers;
