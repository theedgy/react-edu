export default (state = 'list', action = {}) => {
    switch (action.type){
        case 'LAYOUT_CHANGE' :
            return action.layout;

		// no default
    }

    return state;
}
