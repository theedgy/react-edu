export default (state = [], action = {}) => {
    switch (action.type){
		case 'DISPLAY_POSTS' :
            return [ ...action.posts ]

		// no default
    }

    return state;
}
