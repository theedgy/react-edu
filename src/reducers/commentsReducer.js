export default (state = [], action = {}) => {
	switch (action.type){
		case 'ADD_COMMENT' :
			return [ ...state, action.comment ]

		// no default
	}

	return state;
}
