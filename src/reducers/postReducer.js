export default (state = [], action = {}) => {
    switch (action.type){
        case 'GET_POSTS' :
            return [
                ...state,
                ...action.posts
            ]

		// no default
    }

    return state;
}
