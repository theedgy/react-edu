import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Input } from 'reactstrap';

import './style.css';

const LoginForm = ({ userName, password, changeName, changePass, handleSubmit, allowLogin }) => (
	<Form className="login-form">
		<Input type="text" id="userName" placeholder="Username" value={ userName } onChange={ changeName }/>

		<Input type="password" name="password" id="password" placeholder="Password" value={ password } onChange={ changePass }/>

		<Button onClick={ handleSubmit } disabled={ !allowLogin }>Login</Button>
	</Form>
);

LoginForm.propTypes = {
	userName: PropTypes.string,
	password: PropTypes.string,
	changeName: PropTypes.func.isRequired,
	changePass: PropTypes.func.isRequired,
	handleSubmit: PropTypes.func.isRequired,
	allowLogin: PropTypes.bool.isRequired,
};

LoginForm.defaultProps = {
	userName: '',
	password: '',
};

export default LoginForm;
