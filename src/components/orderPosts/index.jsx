import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { defaultParams, URLparams } from '../../helpers';

class OrderPosts extends Component {
	static propTypes = {
		history: PropTypes.object.isRequired
	};

	state = {
		dropdownOpen: false,
		selected: defaultParams.order
	};

	toggle = () => {
		this.setState(prevState => ({
			dropdownOpen: !prevState.dropdownOpen
		}));
	};

	handleChange = (e) => {
		this.setState({
			selected: e.target.value
		});
		this.props.history.push({
			search: `?${ URLparams([ { name: 'order', value: e.target.value } ]).toString() }`
		});
	};

	render() {
		return(
			<Dropdown isOpen={ this.state.dropdownOpen } toggle={ this.toggle }>
				<DropdownToggle caret> Order: { `${ this.state.selected }ending` } </DropdownToggle>
				<DropdownMenu>
					<DropdownItem value='asc' onClick={ this.handleChange }>A - Z</DropdownItem>
					<DropdownItem value='desc' onClick={ this.handleChange }>Z - A</DropdownItem>
				</DropdownMenu>
			</Dropdown>
		)
	}
}
export default withRouter(OrderPosts);
