import React from 'react';

import loader from './Loading_icon.gif';
import './style.css';

const Loader = () => (
	<img src={ loader } className="loader" alt="loader" />
);

export default Loader;
