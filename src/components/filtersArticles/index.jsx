import React from 'react';

import OrderPosts from '../../components/orderPosts';
import SortPostsBy from '../../components/sortPostsBy';
import ChangeLayout from '../../components/changeLayout';
import './style.css'

const FiltersArticles = () => (
	<div className="filters">
		<OrderPosts />
		<SortPostsBy />
		<ChangeLayout />
	</div>
);

export default FiltersArticles;
