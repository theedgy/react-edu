import React from 'react';
import PropTypes from 'prop-types';
import { Pagination, PaginationItem } from 'reactstrap';
import { Link } from 'react-router-dom';

import { URLparams } from '../../helpers';
import './style.css';

const PostsPagination = ({ pages, page }) => (
	<Pagination>
		<PaginationItem disabled={ page === 1 }>
			<Link to={ {
				pathname: `/posts/${ page - 1 }`,
				search: `?${ URLparams().toString() }`
			} }
				  className="page-link"> {'<'} </Link>
		</PaginationItem>

		{ [ ...Array(pages).keys() ].map(key => (
			<PaginationItem active={ page === key+1 } key={ key }>
				<Link to={ {
					pathname: `/posts/${ key+1 }`,
					search: `?${ URLparams().toString() }`
				} }
					  className="page-link" >
					{ key+1 }
				</Link>
			</PaginationItem>
		))}

		<PaginationItem disabled={ page === pages }>
			<Link to={ {
				pathname: `/posts/${ page + 1 }`,
				search: `?${ URLparams().toString() }`
			} }
				  className="page-link"> {'>'} </Link>
		</PaginationItem>
	</Pagination>
);

PostsPagination.propTypes = {
	pages: PropTypes.number.isRequired,
	page: PropTypes.number.isRequired,
};

export default PostsPagination;
