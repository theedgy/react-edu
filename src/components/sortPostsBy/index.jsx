import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { defaultParams, URLparams } from '../../helpers';

class SortPostsBy extends Component {
	static propTypes = {
		history: PropTypes.object.isRequired
	};

	state = {
		dropdownOpen: false,
		selected: defaultParams.sortBy
	};

	toggle = () => {
		this.setState(prevState => ({
			dropdownOpen: !prevState.dropdownOpen
		}));
	};

	handleChange = (e) => {
		this.setState({
			selected: e.target.value
		});

		this.props.history.push({
			search: `?${ URLparams([ { name: 'sortBy', value: e.target.value } ]).toString() }`
		});
	};

	render() {
		return(
			<Dropdown isOpen={ this.state.dropdownOpen } toggle={ this.toggle }>
				<DropdownToggle caret> Sort by: { this.state.selected } </DropdownToggle>

				<DropdownMenu>
					<DropdownItem value='date' onClick={ this.handleChange }>Date</DropdownItem>
					<DropdownItem value='title' onClick={ this.handleChange }>Title</DropdownItem>
				</DropdownMenu>
			</Dropdown>
		)
	}
}

export default withRouter(SortPostsBy);
