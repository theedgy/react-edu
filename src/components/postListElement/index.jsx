import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Col, Button } from 'reactstrap'

import { getPostDate } from '../../helpers';
import './style.css';

class Post extends Component {
	static propTypes = {
		post: PropTypes.object.isRequired,
		handleModal: PropTypes.func.isRequired
	};

	state = {
		excerpt: false
	};

	toggleExcerpt = () => {
		this.setState({
			excerpt: !this.state.excerpt
		})
	};

	render() {
		const { id, title, thumbnail, date, excerpt } = this.props.post;
		const { handleModal } = this.props;

		return(
			<Col xs="12">
				<article className="single-post post-list-element">
					<figure className="post-image">
						<img src={ thumbnail } alt={ title } width="100" />
					</figure>

					<div className="post-content">
						<span>{ getPostDate(date) }</span>
						<h3>
							<Link to={ `/post/${ id }` }> { title } </Link>
						</h3>
					</div>

					<div className="post-triggers">
						<Button color='info' onClick={ this.toggleExcerpt }> e </Button>
						<Button color='warning' onClick={ () => handleModal( id ) } > i </Button>
					</div>

					<div className={ `post-excerpt ${ this.state.excerpt ? 'visible' : '' }` }>
						<p>
							{ excerpt }
							<Button color="danger" onClick={ this.toggleExcerpt }> Close </Button>
						</p>
					</div>
				</article>
			</Col>
		)
	}
}

export default Post;
