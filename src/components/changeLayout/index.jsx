import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { layoutChange } from '../../actions/index';

class ChangeLayout extends Component{
	static propTypes = {
		layout: PropTypes.string.isRequired,
		layoutChange: PropTypes.func.isRequired,
	};

	state = {
		dropdownOpen: false
	};

	toggle = () => {
		this.setState(prevState => ({
			dropdownOpen: !prevState.dropdownOpen
		}));
	};

	handleChange = (e) => this.props.layoutChange({ layout: e.target.value });

	render() {
		return(
			<Dropdown isOpen={ this.state.dropdownOpen } toggle={ this.toggle }>
				<DropdownToggle caret> { this.props.layout } </DropdownToggle>
				<DropdownMenu>
					<DropdownItem value='list' onClick={ this.handleChange }>List</DropdownItem>
					<DropdownItem value='grid' onClick={ this.handleChange }>Grid</DropdownItem>
				</DropdownMenu>
			</Dropdown>
		)
	}
}

const mapStateToProps = state => ({ layout: state.layout });
const mapDispatchToProps = dispatch => ( bindActionCreators({ layoutChange }, dispatch) );

export default connect(mapStateToProps, mapDispatchToProps)(ChangeLayout);
