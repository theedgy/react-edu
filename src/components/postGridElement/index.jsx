import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Col } from 'reactstrap'

import './style.css';

const Post = ({ post }) => (
	<Col xs="6" md="3">
		<article className="single-post post-grid-element">
			<Link to={ `/post/${ post.id }` }>
				<h3>{ post.title }</h3>

				<figure className="post-image">
					<img src={ post.thumbnail } alt={ post.title } />
				</figure>
			</Link>
		</article>
	</Col>
);

Post.propTypes = {
	post: PropTypes.object.isRequired
};

export default Post;
