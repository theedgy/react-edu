import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Container, Button } from 'reactstrap';

import logo from './logo.svg';
import { isLoggedIn } from '../../helpers';
import './style.css';

const Header = ({ handleLogOut }) => (
	<header>

		<Container>
			<img src={ logo } className="App-logo" alt="logo" />

			<Link to='/posts'>Home</Link>

			<h1>Simple Web App</h1>

			{isLoggedIn() && <Button color="link" onClick={ handleLogOut  }> LogOut </Button>}
		</Container>

		<hr/>
	</header>
);

Header.propTypes = {
	handleLogOut: PropTypes.func.isRequired
};

export default Header;
