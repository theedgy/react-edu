import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Label, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from  'redux';

import { addComment } from '../../actions';
import './style.css'

class CommentModal extends Component {
	static propTypes = {
		post: PropTypes.number.isRequired,
		handleToggle: PropTypes.func.isRequired,
		addComment: PropTypes.func.isRequired,
	};

	state = {
		name: '',
		comment: '',
		acceptance: false
	};

	handleSubmit = () => {
		this.props.addComment({
			comment: {
				pID: this.props.post,
				name: this.state.name,
				comment: this.state.comment
			}
		});
		this.props.handleToggle();
	};

	changeContent = (field) => (e) => this.setState({ [ field ] : e.target.value });
	handleAccept =(e) => this.setState({ acceptance : !this.state.acceptance });

	render() {
		const { handleToggle } = this.props;

		return(
			<div className="comment-modal">
				<h4>Add Comment</h4>

				<Input type="text" name="name" id="name" placeholder="Your name" value={ this.state.name } onChange={ this.changeContent('name') }/>

				<Input type="textarea" name="comment" id="comment" placeholder="Your comment" value={ this.state.comment } onChange={ this.changeContent('comment') }/>

				<Label>
					<Input type="checkbox" name="acceptance" checked={ this.state.acceptance } onChange={ this.handleAccept } />{' '}I Accept
				</Label>

				<div className="comment-modal__buttons">
					<Button color="secondary" onClick={ this.handleSubmit } disabled={ !this.state.acceptance }>Submit</Button>
					<Button color="secondary" onClick={ handleToggle }>Close</Button>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({ comments: state.comments });
const mapDispatchToProps = dispatch => ( bindActionCreators({ addComment }, dispatch) );

export default connect(mapStateToProps, mapDispatchToProps)(CommentModal);
