import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Modal } from 'reactstrap';

class ModalElement extends Component {
	static propTypes = {
		open: PropTypes.bool,
		children: PropTypes.node.isRequired
	};

	render() {
		return ReactDOM.createPortal(
			<Modal isOpen={ this.props.open }>
				{ this.props.children }
			</Modal>,
			document.getElementById('modal')

		);
	}
}

export default ModalElement;
