import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

import './style.css'

const AuthorInfo = ({ author, handleToggle }) => (
	<div className="author-info">
		<div className="author-info__header">
			<h4 className="name">{ author.name }</h4>
			<img src={ author.avatar } alt={ author.name } width="150"/>
		</div>

		<p>{ author.description }</p>

		<Button color="secondary" onClick={ () => handleToggle(false) }>Close</Button>
	</div>
);

AuthorInfo.propTypes = {
	author: PropTypes.object.isRequired,
	handleToggle: PropTypes.func.isRequired
};

export default AuthorInfo;
