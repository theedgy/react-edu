export const updatePosts = ({ posts }) => (
    {
        type: 'GET_POSTS',
        posts
    }
);

export const updateDisplayPosts = ({ posts }) => (
    {
        type: 'DISPLAY_POSTS',
        posts
    }
);

export const layoutChange = ( layout ) => (
    {
        type: 'LAYOUT_CHANGE',
		...layout
    }
);

export const addComment = ({ comment }) => (
    {
        type: 'ADD_COMMENT',
		comment
    }
);
