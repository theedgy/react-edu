import { apiUrl } from '../services/API';

const axios = require('axios');

export const URLparams = (params = undefined, searchString = window.location.search) => {
	const searchParams = new URLSearchParams(searchString);

	if (params) {
		params.map(param => (
			searchParams.set(param.name, param.value)
		));
	}

	return searchParams
};

//Default values for fetching
export const defaultParams = {
	limit: 10,
	page: 1,
	order: URLparams().get('order') ? URLparams().get('order') : 'desc',
	sortBy: URLparams().get('sortBy') ? URLparams().get('sortBy') : 'title'
};

export const authApp = () => axios.post(`${ apiUrl }/auth`);
export const getPosts = (fetchParams) => axios.get(`${ apiUrl }/posts`, { params: Object.assign({}, defaultParams, fetchParams ) });
export const getPost = (id) => axios.get(`${ apiUrl }/posts/${ id }`);
export const sendTime = (id, time) => axios.put(`${ apiUrl }/time/${ id }`, { params: { time } });

export const getPostDate = date => {
	const d = new Date(date);
	return `${ d.getDate() }.${ d.getMonth()+1 }.${ d.getFullYear() }`
};

export const setCookie = (cname, cvalue, exdays) => {
	const d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	const expires = `expires=${ d.toUTCString() }`;
	document.cookie = `${ cname }=${ cvalue };${ expires };path=/`;
};

export const getCookie = (cname) => {
	const name = `${ cname }=`;
	const decodedCookie = decodeURIComponent(document.cookie);
	const ca = decodedCookie.split(';');
	for(let i = 0; i <ca.length; i++) {
		let c = ca[ i ];
		while (c.charAt(0) === ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
};

export const deleteCookie = (cname) => {
	document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
};

export const isLoggedIn = () => getCookie('token');
