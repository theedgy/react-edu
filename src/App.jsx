import React, { Component, Fragment } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Header from './components/header';
import Login from './containers/loginPage';
import Articles from './containers/articles';
import Post from './containers/post'
import { updatePosts } from './actions/index';
import { getPosts, deleteCookie } from './helpers';

import './App.css';

class App extends Component {
	static propTypes = {
		allPosts: PropTypes.array.isRequired,
		updatePosts: PropTypes.func.isRequired,
	};

	componentDidMount = () => {
		!this.props.allPosts.length > 0 && getPosts({ limit: -1 }).then(response => (
				this.props.updatePosts({ posts: response.data })
			)
		);
	};

	logOut = () => {
		deleteCookie('token');
		this.forceUpdate();
	};

	render() {
		return (
			<BrowserRouter>
				<Fragment>
					<Header handleLogOut={ this.logOut }/>
					<div id="modal"/>

					<Switch>
						<Route exact path="/" component={ Login }/>
						<Route exact path="/posts/" component={ Articles }/>
						<Route path="/posts/:page" component={ Articles }/>
						<Route path="/post/:id" component={ Post }/>
					</Switch>
				</Fragment>
			</BrowserRouter>
		)
	}
}

const mapStateToProps = state => ({ allPosts: state.allPosts });
const mapDispatchToProps = dispatch => ( bindActionCreators({ updatePosts }, dispatch) );

export default connect(mapStateToProps, mapDispatchToProps)(App);
